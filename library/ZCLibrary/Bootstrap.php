<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 24/8/15
 * Time: 18:27
 */

namespace ZCLibrary;


class Bootstrap
{

    public function dispatch()
    {
        include BASE_PATH . '/services.php';
        $parts = explode('/', $_SERVER['REQUEST_URI']);
        $controllerName = 'ZCLibrary\\Controller\\'.ucfirst($parts[2]) . 'Controller';
        $actionName = strtolower($parts[3]) . 'Action';

        $dispatcher = new $controllerName();
        $dispatcher->$actionName();
    }
}