<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 25/8/15
 * Time: 20:25
 */

namespace ZCLibrary\Controller;


use ZCLibrary\Controller;
use ZCLibrary\DependencyInjection\Request;
use ZCLibrary\DependencyInjection\Session;
use ZCLibrary\DependencyInjection\User;
use ZCLibrary\Form\Login;
use ZCLibrary\Form\Register;

/**
 * Class UserController
 * @package ZCLibrary\Controller
 */
class UserController extends Controller
{
    /**
     * Handling the registration feature.
     *
     * @throws \ZCLibrary\DependencyInjection\Exception\NotFoundService
     */
    public function registerAction()
    {
        /** @var Request $request */
        $request = $this->di->get('request');
        /** @var User $users */
        $users = $this->di->get('users');

        if (!$request->isPost()) {
            $this->render('register');
        } else {
            $form = new Register($request->getParams());

            if (!$form->isValid()) {
                $this->render('register', array('errors' => $form->getErrors()));
            } else {
                $users->register($request->getParam('name'), $request->getParam('email'), md5($request->getParam('password1')));
                $this->redirect('/index.php/user/login');
            }
        }
    }

    /**
     * Handling the login feature.
     *
     * @throws \ZCLibrary\DependencyInjection\Exception\NotFoundService
     */
    public function loginAction()
    {
        /** @var Request $request */
        $request = $this->di->get('request');

        if (!$request->isPost()) {
            $this->render('login');
        } else {
            $form = new Login($request->getParams());
            if (!$form->isValid()) {
                $this->render('login', array('errors' => $form->getErrors()));
            } else {
                /** @var Session $session */
                $session = $this->di->get('session');
                $session->add('authenticated', true);
                $this->redirect('/index.php/user/list');
            }
        }
    }

    /**
     * Handling the list of users feature.
     *
     * @throws \ZCLibrary\DependencyInjection\Exception\NotFoundService
     */
    public function listAction()
    {
        /** @var Session $session */
        $session = $this->di->get('session');

        if (false === $session->get('authenticated')) {
            $this->redirect('/index.php/user/login');
        }

        /** @var Request $request */
        $request = $this->di->get('request');
        if (!$request->isPost()) {
            $this->render('list');
        } else {
            $query = $request->getParam('q');
            $users = $this->di->get('users')->search($query);

            $this->render('list', array('results' => $users));
        }
    }

    /**
     * Handling home feature ...
     */
    public function homeAction()
    {
        $this->render('home');
    }
}