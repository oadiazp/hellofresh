<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 25/8/15
 * Time: 19:04
 */

namespace ZCLibrary;

/**
 * Base class for the exceptions
 *
 * Class Exception
 * @package ZCLibrary
 */
class Exception extends \Exception
{

}