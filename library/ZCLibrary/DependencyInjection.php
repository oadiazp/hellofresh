<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 25/8/15
 * Time: 18:52
 */

namespace ZCLibrary;

use ZCLibrary\DependencyInjection\Exception\NotFoundService;

/**
 * Class DependencyInjection
 * @package ZCLibrary
 */
class DependencyInjection
{
    /**
     * @var array
     */
    private $services;

    /**
     * @var DependencyInjection
     */
    private static $instance;

    /**
     * Disabling the capability of create an instance of this class.
     */
    private function __construct()
    {
        $this->services = array();
        self::$instance = null;
    }

    /**
     * Disabling the clone for this class, always will receive the same instance
     *
     * @return DependencyInjection
     */
    public function __clone()
    {
        return self::$instance;
    }

    /**
     * Entry point for the Singleton pattern
     *
     * @return DependencyInjection
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Add a service into the container
     *
     * @param string $name
     * @param DependencyInjection $instance
     * @return DependencyInjection
     */
    public function add($name, $instance)
    {
        $this->services[$name] = $instance;

        return self::$instance;
    }

    /**
     * Get a service from the container
     *
     * @param string $name
     * @return mixed
     * @throws NotFoundService
     */
    public function get($name)
    {
        if (!isset($this->services[$name])) {
            throw new NotFoundService();
        }

        return $this->services[$name];
    }

    /**
     * Shorthand for the get method
     * @param string $service
     * @return mixed
     * @throws NotFoundService
     */
    public function __get($service)
    {
        return $this->get($service);
    }
}