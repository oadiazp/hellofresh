<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 24/8/15
 * Time: 19:02
 */

namespace ZCLibrary;

/**
 * Controller base class
 *
 * Class Controller
 * @package ZCLibrary
 */
class Controller
{
    /**
     * @var DependencyInjection
     */
    protected $di;

    public function __construct()
    {
        $this->di = DependencyInjection::getInstance();
    }

    /**
     * @param string $view
     * @param array $variables
     */
    public function render($view, array $variables = array())
    {
        foreach ($variables as $key => $value) {
            $$key = $value;
        }

        include(BASE_PATH . '/view/' . $view . '.php');
    }

    /**
     * @param string $url
     */
    public function redirect($url)
    {
        header('Location: ' . $url);
    }
}
