<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 27/8/15
 * Time: 18:45
 */

namespace ZCLibrary\Validate;


use ZCLibrary\DependencyInjection;

/**
 * Class Authentication
 * Validates if the authentication was passed
 *
 * @package ZCLibrary\Validate
 */
class Authentication extends AbstractValidate
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    public function __construct($email, $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    public function validate()
    {
        /** @var DependencyInjection\User $users */
        $users = DependencyInjection::getInstance()->get('users');

        return $users->login($this->email, $this->password);
    }
}