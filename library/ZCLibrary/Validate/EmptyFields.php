<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 26/8/15
 * Time: 15:48
 */

namespace ZCLibrary\Validate;

/**
 * Class EmptyFields
 *
 * @package ZCLibrary\Validate
 */
class EmptyFields extends AbstractValidate
{
    private $values;

    public function __construct(array $array)
    {
        $this->values = $array;
    }

    public function validate()
    {
        foreach ($this->values as $key => $value) {
            if ($value == "") {
                return false;
            }
        }

        return true;
    }
}