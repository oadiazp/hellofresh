<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 26/8/15
 * Time: 15:41
 */

namespace ZCLibrary\Validate;

/**
 * Class MatchingPasswords
 * @package ZCLibrary\Validate
 */
class MatchingPasswords extends AbstractValidate
{
    private $password1;
    private $password2;

    public function __construct($password1, $password2)
    {
        $this->password1 = $password1;
        $this->password2 = $password2;
    }

    public function validate()
    {
        return $this->password1 === $this->password2;
    }
}