<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 25/8/15
 * Time: 22:52
 */

namespace ZCLibrary\Validate;


use ZCLibrary\DependencyInjection;

/**
 * Class UniqueEmail
 * @package ZCLibrary\Validate
 */
class UniqueEmail extends AbstractValidate
{
    private $email;

    public function __construct($email)
    {
        $this->email = $email;
    }

    public function validate()
    {
        /** @var DependencyInjection\User $users */
        $users = DependencyInjection::getInstance()->get('users');
        return $users->checkUniqueEmail($this->email);
    }
}