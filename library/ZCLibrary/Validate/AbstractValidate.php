<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 26/8/15
 * Time: 22:07
 */

namespace ZCLibrary\Validate;

/**
 * Class AbstractValidate
 * Abstract validation class.
 *
 * @package ZCLibrary\Validate
 */
abstract class AbstractValidate
{
    public abstract function validate();
}