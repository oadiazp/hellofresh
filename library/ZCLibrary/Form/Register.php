<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 27/8/15
 * Time: 12:01
 */

namespace ZCLibrary\Form;


use ZCLibrary\Validate\EmptyFields;
use ZCLibrary\Validate\MatchingPasswords;
use ZCLibrary\Validate\UniqueEmail;

class Register extends AbstractForm
{
    public function __construct(array $values)
    {
        parent::__construct($values);

        $this->getValidator()
             ->add(new UniqueEmail($values['email']), 'This email address already exists')
             ->add(new MatchingPasswords($values['password1'], $values['password2']), 'Passwords are different')
             ->add(new EmptyFields($values), 'All fields are required');

    }
}