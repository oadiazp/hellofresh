<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 27/8/15
 * Time: 10:56
 */

namespace ZCLibrary\Form;


use ZCLibrary\DependencyInjection;
use ZCLibrary\DependencyInjection\Validate;

class AbstractForm
{
    /** @var Validate */
    private $validator;
    /** @var array */
    private $errors;
    /** @var array */
    private $values;
    /** @var array */
    private $fields;

    public function __construct(array $values)
    {
        $this->validator = DependencyInjection::getInstance()->get('validator');
        $this->errors = array();
        $this->values = $values;
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param array $fields
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
    }



    /**
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param array $values
     */
    public function setValues($values)
    {
        $this->values = $values;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        $this->errors = $this->validator->validate();

        return count($this->errors) === 0;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return Validate
     */
    public function getValidator()
    {
        return $this->validator;
    }
}