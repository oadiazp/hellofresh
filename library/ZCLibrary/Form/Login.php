<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 27/8/15
 * Time: 17:58
 */

namespace ZCLibrary\Form;


use ZCLibrary\Validate\Authentication;
use ZCLibrary\Validate\EmptyFields;

class Login extends AbstractForm
{

    /**
     * Login constructor.
     */
    public function __construct(array $values)
    {
        parent::__construct($values);

        $this->getValidator()
             ->add(new EmptyFields($values), 'All fields are required')
             ->add(new Authentication($values['email'], $values['password']), 'Failed authentication');
    }
}