<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 30/8/15
 * Time: 9:20
 */

namespace ZCLibrary\Test;

use PHPUnit_Framework_TestCase;
use ZCLibrary\DependencyInjection;
use ZCLibrary\Exception;

class DependencyInjectionTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var DependencyInjection
     */
    private $di;

    public function testAddService()
    {
        $di = $this->di->add('config',
                             new DependencyInjection\Config(BASE_PATH . '/config/settings.ini'));
        $this->assertInstanceOf('ZCLibrary\\DependencyInjection', $di);
    }

    public function testGetAnExistingService()
    {
        $this->assertInstanceOf('ZCLibrary\\DependencyInjection\\Config',
                                 $this->di->get('config'));
    }

    public function testGetAMissingService()
    {
        try {
            $this->di->get('missingService');
        }
        catch (Exception $exception) {
            $this->assertInstanceOf('ZCLibrary\\DependencyInjection\\Exception\\NotFoundService', $exception);
        }
    }


    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->di = DependencyInjection::getInstance();
    }
}

