<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 25/8/15
 * Time: 22:30
 */

namespace ZCLibrary\DependencyInjection;


use ZCLibrary\Validate\AbstractValidate;

/**
 * Class Validate
 * Handles the validation
 *
 * @package ZCLibrary\DependencyInjection
 */
class Validate
{
    private $validators;

    public function __construct()
    {
        $this->validators = array();
    }

    /**
     * @param AbstractValidate $instance
     * @param string $errorMessage
     * @return $this
     */
    public function add(AbstractValidate $instance, $errorMessage)
    {
       $this->validators[] = array(
           'instance' => $instance,
           'message' => $errorMessage
       );

       return $this;
    }

    public function validate()
    {
        $result = array();

        foreach ($this->validators as $validator) {
            if (false === $validator['instance']->validate()) {
                $result[] = $validator['message'];
            }
        }

        return $result;
    }
}