<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 25/8/15
 * Time: 19:34
 */

namespace ZCLibrary\DependencyInjection;

/**
 * Class Database
 * Handling the database access.
 *
 * @package ZCLibrary\DependencyInjection
 */
class Database
{
    private $connection;

    public function __construct(Config $config)
    {
        $array = $config->get('database');
        $dsn = sprintf('mysql:host=%s;dbname=%s', $array['host'], $array['name']);
        $this->connection = new \PDO($dsn, $array['username'], $array['password']);
    }

    /**
     * Executes a DML query returning the result.
     *
     * @param string $sql
     * @return \PDOStatement
     */
    public function query($sql)
    {
        return $this->connection->query($sql, \PDO::FETCH_ASSOC);
    }

    /**
     * Executes a DML that don't expect for results (INSERT)
     *
     * @param string $sql
     * @return int
     */
    public function exec($sql)
    {
        return $this->connection->exec($sql);
    }
}