<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 25/8/15
 * Time: 19:12
 */

namespace ZCLibrary\DependencyInjection;


/**
 * Class Config
 * Handling the configuration into the project.
 *
 * @package ZCLibrary\DependencyInjection
 */
class Config
{
    private $settings;

    public function __construct($filename)
    {
        $this->settings = parse_ini_file($filename, true);
    }

    public function get($setting)
    {
        return $this->settings[$setting];
    }
}