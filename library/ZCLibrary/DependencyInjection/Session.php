<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 27/8/15
 * Time: 19:58
 */

namespace ZCLibrary\DependencyInjection;

/**
 * Class Session
 * Handling the session
 *
 * @package ZCLibrary\DependencyInjection
 */
class Session
{
    private $namespace;

    public function __construct(Config $config)
    {
        session_start();

        $sessionConfig = $config->get('session');
        $this->namespace = $sessionConfig['namespace'];
        $this->flash = array();

        if (!isset($_SESSION[$this->namespace])) {
            $_SESSION[$this->namespace] = array();
            $_SESSION[$this->namespace]['flash'] = array();
        }
    }

    public function add($key, $value)
    {
        $_SESSION[$this->namespace][$key] = $value;
    }

    public function get($key)
    {
        if (isset($_SESSION[$this->namespace][$key])) {
            return $_SESSION[$this->namespace][$key];
        } else {
            return false;
        }
    }
}