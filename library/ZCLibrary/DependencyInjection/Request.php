<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 25/8/15
 * Time: 20:27
 */

namespace ZCLibrary\DependencyInjection;


/**
 * Class Request
 * Handles the request
 *
 * @package ZCLibrary\DependencyInjection
 */
class Request
{
    public function isPost()
    {
        return 'POST' === strtoupper($_SERVER['REQUEST_METHOD']);
    }

    public function getParam($parameter)
    {
        if ($this->isPost()) {
            return $_POST[$parameter];
        } else {
            return $_GET[$parameter];
        }
    }

    public function getKeys()
    {
        if ($this->isPost()) {
            return array_keys($_POST);
        } else {
            return array_keys($_GET);
        }
    }

    /**
     * @return array
     */
    public function getParams()
    {
        if ($this->isPost()) {
            return $_POST;
        } else {
            return $_GET;
        }
    }

    public function keyExists($key)
    {
        if ($this->isPost()) {
            return isset($_POST[$key]);
        } else {
            return isset($_GET[$key]);
        }
    }
}