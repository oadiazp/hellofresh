<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 25/8/15
 * Time: 20:50
 */

namespace ZCLibrary\DependencyInjection;

/**
 * Class User
 * Handles the business logic
 *
 * @package ZCLibrary\DependencyInjection
 */
class User
{
    /** @var Database */
    private $db;

    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    /**
     * Register the user
     *
     * @param string $name
     * @param string $email
     * @param string $password
     * @return int
     */
    public function register($name, $email, $password)
    {
        $sql = 'INSERT INTO users (name, password, email) VALUES ("%s", "%s", "%s")';
        $query = sprintf($sql, $name, $password, $email);
        return $this->db->exec($query);
    }

    /**
     * Check if the user/password is valid
     *
     * @param string $email
     * @param string $password
     * @return bool
     */
    public function login($email, $password)
    {
        $sql = 'SELECT COUNT(*) AS cant FROM users WHERE email = "%s" AND password = "%s"';
        $query = sprintf($sql, $email, md5($password));
        $result = $this->db->query($query)->fetchAll();
        return $result[0]['cant'] === '1';
    }

    /**
     * Makes the search
     *
     * @param string $parameter
     * @return array
     */
    public function search($parameter)
    {
        $sql = "SELECT * FROM users WHERE name LIKE '%{$parameter}%' OR email LIKE '%{$parameter}%'";
        return $this->db->query($sql)->fetchAll();
    }

    /**
     * Check if that email is unique
     *
     * @param string $email
     * @return bool
     */
    public function checkUniqueEmail($email)
    {
        $sql = 'SELECT COUNT(*) AS cant FROM users WHERE email = "%s"';
        $query = sprintf($sql, $email);

        $result = $this->db->query($query)->fetchAll();
        return $result[0]['cant'] === "0";
    }
}