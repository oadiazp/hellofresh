<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 25/8/15
 * Time: 19:23
 */

$di = \ZCLibrary\DependencyInjection::getInstance();

$di->add('config',
    new \ZCLibrary\DependencyInjection\Config(BASE_PATH . '/config/settings.ini'))
    ->add('db',
    new \ZCLibrary\DependencyInjection\Database($di->get('config')))
    ->add('request',
    new \ZCLibrary\DependencyInjection\Request())
    ->add('users',
    new \ZCLibrary\DependencyInjection\User($di->get('db')))
    ->add('validator',
    new \ZCLibrary\DependencyInjection\Validate())
    ->add('session',
    new \ZCLibrary\DependencyInjection\Session($di->get('config')));