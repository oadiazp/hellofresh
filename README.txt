Technical description of the solution
=====================================

The idea behind this application architecture is to provide an microframework that solves all the specified features.

The main architectural pattern is the Model-View-Controller (MVC) but in the common scenarios is very common those components can be very copulated so we need a dependency container (DI) component that holds all the dependencies together and hides to other components its contracts.

Every incoming request has the same entry point (Front controller) an static part of code that only bootstrap the system and routes the request to the controller considering the parameters passed in the URL.

http://<host>/index.php/<controller>/<action>

That loads the the <controller>Controller and calls to the <action>Action method in that controller class.

The dependency container has some services like:

- Config: parses a config file (settings.ini) and allows to parametrize another dependencies like the database access (connection settings) or the session one (namespace).
- Database: Hides all the database interaction to the other components.
- Validation: Create the validation elements that evaluates the information that comes from the user. The forms are the components made for connect all the validator putting that all together.
- Session: This component allows to keep user-information alive between multiple requests.
- User: This component encapsulates all the bussiness logic.