<html>
<header>
    <title>Test</title>
</header>
<body>
<h3>Login</h3>

<?php if (isset($errors)):?>
    <ul>
        <?php foreach ($errors as $error): ?>
            <li><?php echo $error; ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif;?>

<form method="post">
    <label for="email">Email:</label>
    <input type="email" name="email"/>
    <label for="password">Password:</label>
    <input type="password" name="password"/>
    <button type="submit">Submit</button>
</form>
</body>
</html>
