<html>
<head>
    <title>Test</title>
</head>
<body>
    <h2>Results</h2>
    <form method="post">
        <label for="q">Search:</label>
        <input name="q" type="text"/>
        <button>Submit</button>
    </form>
    <table>
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
            <?php if (isset($results)):?>
                <?php foreach ($results as $user):?>
                    <tr>
                        <td>
                            <?php echo $user['name']; ?>
                        </td>
                        <td>
                            <?php echo $user['email']; ?>
                        </td>
                    </tr>
                <?php endforeach ?>
            <?php endif?>
        </tbody>
    </table>
</body>
</html>