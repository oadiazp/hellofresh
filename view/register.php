<html>
<header>
    <title>Test</title>
</header>
<body>
    <h3>Register</h3>

    <?php if (isset($errors)):?>
        <ul>
            <?php foreach ($errors as $error): ?>
                <li><?php echo $error; ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif;?>

    <form method="post">
        <label for="name">Name:</label>
        <input type="text" name="name"/>
        <label for="email">Email:</label>
        <input type="email" name="email"/>
        <label for="password1">Password:</label>
        <input type="password" name="password1"/>
        <label for="password2">Repeat password:</label>
        <input type="password" name="password2"/>
        <button type="submit">Submit</button>
    </form>
</body>
</html>
