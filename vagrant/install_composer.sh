#!/usr/bin/env bash

wget https://phar.phpunit.de/phpunit.phar
chmod +x phpunit.phar
sudo mv phpunit.phar /usr/local/bin/phpunit
curl -sS https://getcomposer.org/installer | php
cd /vagrant
php composer.phar install