CREATE TABLE users (
  id       int(10) NOT NULL AUTO_INCREMENT,
  name     varchar(200),
  password varchar(255),
  email    varchar(200),
  PRIMARY KEY (id));