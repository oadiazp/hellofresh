#!/usr/bin/env bash
echo "mysql-server-5.5 mysql-server/root_password password root" | debconf-set-selections
echo "mysql-server-5.5 mysql-server/root_password_again password root" | debconf-set-selections
sudo apt-get -qy install mysql-server-5.5
sudo apt-get install -y language-pack-en language-pack-de apache2 php5 php5-mysql
mysql -uroot -e "CREATE DATABASE hellofresh";
mysql -uroot hellofresh < /vagrant/vagrant/create.sql
cp /vagrant/vagrant/vhost /etc/apache2/sites-available/vhost.conf
sudo a2ensite vhost
sudo service apache2 restart
sudo service mysql restart
cd /vagrant
sh /vagrant/vagrant/install_composer.sh
phpunit
