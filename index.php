<?php
include "autoloader.php";

try {
    $bootstrap = new ZCLibrary\Bootstrap();
    $bootstrap->dispatch();
}
catch (\ZCLibrary\Exception $exception) {
    echo 'Exception: ' . $exception->getMessage();
}
