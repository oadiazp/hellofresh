<?php
/**
 * Created by PhpStorm.
 * User: zcool
 * Date: 24/8/15
 * Time: 19:59
 */

define('BASE_PATH', realpath(dirname(__FILE__)));
function autoloader($class)
{
    if (false === strpos($class, 'ZCLibrary')) {
        return false;
    }

    $filename = BASE_PATH . '/library/' . str_replace('\\', '/', $class) . '.php';
    include($filename);
}
spl_autoload_register('autoloader');
include 'vendor/autoload.php';